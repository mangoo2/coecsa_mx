<?php
    $owner_email = 'contacto@coecsa.mx';
    $headers  = "MIME-Version: 1.0\r\n";
    $headers .= "Content-type: text/html; charset=UTF-8\r\n";
    $headers .= "From: ".$_POST["email"];
    $headers .= "Reply-To: ".$_POST["email"]."\r\n";
    $subject = 'Mensaje de un visitante web... ';
    $messageBody = "";
    $messageBody .= 'Visitante: ' . $_POST["nombre"]  . "<br>\n";
    $messageBody .= "\n";
    $messageBody .= 'Correo: ' . $_POST['email'] . "<br>\n";
    $messageBody .= "\n";
    $messageBody .= 'Comentario: ' . $_POST['mensaje'] . "\n";
    if(isset($_POST["stripHTML"]) == 'true'){$messageBody = strip_tags($messageBody);}
    try{
        if(!@mail($owner_email, $subject, $messageBody, $headers)){
            throw new Exception('mail failed');
        }else{
            echo '<script language="JavaScript"> alert(\'¡Gracias por contactarnos, en breve nos comunicaremos contigo!\'); </script>';
            header('Location: https://coecsa.mx/contacto.html');
        }
    }catch(Exception $e){
        echo '<script language="JavaScript"> alert(\'No ha sido posible enviar su mensaje, Intente nuevamente por favor.\'); </script>';
        //header('Location: /index.html#contacto');
        header('Location: https://coecsa.mx/contacto.html');
    }

    //  